# List of songs

+ Hot N Cold - Katy Perry
+ Who Let the Dogs Out? - Baha Men (The Sunlight Shakers)
+ Cosmic Girl - Jamiroquai
+ TiK ToK - Ke$ha
+ Lollipop - Mika
+ Don’t Stop Me Now - Queen
+ Single Ladies (Put a Ring on It) - Beyonce
+ Five Little Monkeys - The Just Dance Kids
+ Despacito - Luis Fonsi ft. Daddy Yankee
+ Superstition - Stevie Wonder

[Here you can find the complete list of songs available](https://justdance.fandom.com/wiki/Just_Dance_Unlimited)